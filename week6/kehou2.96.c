#include <stdio.h>
#include <stdlib.h>
#include <math.h>
typedef unsigned float_bits;
typedef unsigned char* bit_transform;
unsigned swift(bit_transform b);
int float_f2i(float_bits f);
int main()
{
unsigned x=0x80000000;
float f=-9.8;
printf("%d\n",float_f2i(swift((bit_transform)&f)));
printf("%d",(unsigned)f);
printf("\nHello world!\n");
return 0;
}
unsigned swift(bit_transform b)
{
    unsigned un=0;
    unsigned sign[4];
    sign[0]=(unsigned)b[0];
    sign[1]=((unsigned)b[1])<<8;
    sign[2]=((unsigned)b[2])<<16;
    sign[3]=((unsigned)b[3])<<24;
    un=sign[0]|sign[1]|sign[2]|sign[3];
    printf("%x %x %x %x\n", b[3],b[2],b[1],b[0]);
    return un;
}
 int float_f2i(float_bits f)
{
    unsigned sign=f>>31;
    unsigned exp=f>>23&0xFF;
    int exp_E=(int)exp-127;
    unsigned frac=f&0x7FFFFF;
    frac=frac|0x800000;
    if(exp==0)return f&0x80000000;
    if(exp_E>30)return 0x80000000;
    if(exp_E<0)return 0;
    if(sign==1)return -(int)(frac*pow(2,exp_E-23));
    return (int)(frac*pow(2,exp_E-23));
}
