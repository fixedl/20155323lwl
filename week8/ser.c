#define _FILE_OFFSET_BITS 64  
  
#include <stdlib.h>  
#include <stdio.h>  
#include <errno.h>  
#include <string.h>  
#include <unistd.h>  
#include <netdb.h>  
#include <sys/socket.h>  
#include <netinet/in.h>  
#include <sys/types.h>  
#include <arpa/inet.h>  
#include <fcntl.h>  
#include <pthread.h>
  
#define PACK_SIZE 1024*512  
  
int main(int argc, char *argv[])  
{  
    setvbuf(stdout, NULL, _IONBF, 0);  
    fflush(stdout);  
  
    int sockfd,new_fd;  
    struct sockaddr_in server_addr;  
    struct sockaddr_in client_addr;  
    int sin_size,portnumber;  
    char hello[]="Hello! Are You Fine?\n";  
  
    if((portnumber=atoi("155323"))<0)  
    {  
        fprintf(stderr,"Usage:%s portnumber\a\n",argv[0]);  
        exit(1);  
    }  
  
    if((sockfd=socket(AF_INET,SOCK_STREAM,0))==-1) {  
        fprintf(stderr,"Socket error:%s\n\a",strerror(errno));  
        exit(1);  
    }  
 
    bzero(&server_addr,sizeof(struct sockaddr_in));  
    server_addr.sin_family=AF_INET;  
    server_addr.sin_addr.s_addr=htonl(INADDR_ANY);  
    server_addr.sin_port=htons(portnumber);  

    if(bind(sockfd,(struct sockaddr *)(&server_addr),sizeof(struct sockaddr))==-1) {  
        fprintf(stderr,"Bind error:%s\n\a",strerror(errno));  
        exit(1);  
    }  
 
    if(listen(sockfd,5)==-1) {  
        fprintf(stderr,"Listen error:%s\n\a",strerror(errno));  
        exit(1);  
    }  
  
    while(1)  
    {  
        fprintf(stderr, "server is listening!\n");  
  
        sin_size=sizeof(struct sockaddr_in);  
        if( ( new_fd = accept(sockfd,(struct sockaddr *)(&client_addr),(socklen_t*)&sin_size ) ) == -1) {  
            fprintf(stderr,"Accept error:%s\n\a",strerror(errno));  
            exit(1);  
        }  
  
        fprintf(stderr,"Server get connection from %s\n",  
            inet_ntoa(client_addr.sin_addr));  
        if(write(new_fd,hello,strlen(hello))==-1) {  
            fprintf(stderr,"Write Error:%s\n",strerror(errno));  
            exit(1);  
        }  
  
        long int read_size = 0;  
        unsigned long file_len  = 0;  
        int order_id  = 0;  
        char file_name[128] = {'\0'};  
        char file_info[1024] = {'\0'};  

        printf("\n\nWaiting for read file info!\n");  
        int nn = 0;  
        if(nn = read(new_fd, file_info, 1024))   
        {  
   
            int id_h = (int)file_info[0]<<8;  
            order_id = id_h + (int)file_info[1];  
 
            unsigned long len_hig_1 = 0;  
            memcpy(&len_hig_1, &file_info[2], sizeof(file_info[2]));  
  
            unsigned long len_hig_2 = 0;  
            memcpy(&len_hig_2, &file_info[3], sizeof(file_info[3]));  
  
            unsigned long len_hig = len_hig_1 * 256 + len_hig_2;  
  
            unsigned long len_low_1 = 0;  
            memcpy(&len_low_1, &file_info[4], sizeof(file_info[4]));  
  
            unsigned long len_low_2 = 0;  
            memcpy(&len_low_2, &file_info[5], sizeof(file_info[5]));  
  
            int len_low = len_low_1 * 256 + len_low_2;  
            file_len = len_hig * 256 * 256 + len_low;  
  
            strncpy(file_name, &file_info[6], strlen(&file_info[6]));  
  
            printf("order = %d, %lu, %s\n", order_id, file_len, file_name);  
  
            if((strlen(file_name) == 0) || (file_len == 0))  
            {  
                printf("Read file info error!\n File_name or file_len is zero!\n");  
                close(new_fd);  
                continue;  
            }  
        }  
        else {  
            printf("Read file info error!\n");  
            close(new_fd);  
            close(sockfd);  
            exit(0);  
        }  
  
        printf("\n\nWaiting for read file content!\n");  
        FILE* pf = fopen(file_name, "wb+");  
        if(pf == NULL)  
        {  
            printf("Open file error!\n");  
            close(new_fd);  
            continue;  
        }  
  
        char buff[PACK_SIZE] = {'\0'};
        int isLastChar = 1;
        char a;
        int i,count=0;
        while(read_size <= file_len) {  
  
            int rlen = read(new_fd, buff, PACK_SIZE);  
            if(rlen) {
                for (i=1;i<rlen;i++)
                {
		   a = buff[i];

		   if (a==' '||a=='\t'||a=='\0')
		   {
		        !isLastChar && count++;
			isLastChar =1;	  
		   }
		} 
  
                printf("\n\nRead package size = %d\n", rlen);  
  
                int wn = fwrite(buff, sizeof(char), rlen, pf);  
                read_size += rlen;  
  

            }  
            else {  
 
                break;  
            }  
        } 

 
        fclose(pf);  
        close(new_fd);  
    }  
  
    close(sockfd);  
    exit(0);  
}  
